title: Some interesting things to read in the first week of march 2018 (mostly from mastodon) and some spare notes
date: 2018-03-08 11:50
tags: mastodon, things to read, things to do 
---

# Things to read

## Papers we love

The [Papers we love](https://mastodon.social/web/accounts/54489) account on Mastodon is one to follow.

Recently they have published  interesting papers:

  * [one](https://mstdn.io/@paperswelove/99639722073063558) about Rsync. Incidentally, I am using Rsync for this blog. 
  * [another one](https://mstdn.io/@paperswelove/99639727060443968) about hash functions internals

## other sources (but from Mastodon anyway)

On the intersection between technology and politics (and psichology) an [interesting paper](https://coolguy.website/writing/the-future-will-be-technical/background().html), although not linear (!), by the [Scuttlebut](https://www.scuttlebutt.nz/) authors.

Scuttlebut is an extremely interesting project. Too bad it's made in JavaScritp and the npm packages are a headache for packagers right now, as I wrote in my opening post.

I read a few paragraphs now and they seems to be inspired. Especially the one about the typical conversation with your friends about not mainstream social networks.

"The future will be technical" deeply resonates within me. I'm following a human rights organization ( [in English](https://www.nrptt.org/) and [in Italian](http://radicalparty.org/it)) and they are classically humanists. They don't get the tech dimension at all and they don't get how much this makes them marginal.

It's tragic. They denounce the deterioration of the democratic standards but they don't see a possible way forward.

So I reccommend this paper by the Scuttlebut people

### Videos, but not on Youtube

I discovered that you can publish a footage of yours hosting it NOT on Youtbe or Vimeo but rather on archive.org !

An example [here](https://mastodon.sdf.org/@art/99616093857191321)

And this discovery was thanks to [this](https://mastodon.sdf.org/@art/99617414419148739) toot

This is indeed very interesting. But I don't understand how that works. 

How does archive.org deal with storage space and bandwith ? If many users start hosting their footages there, won't they bring archive.org to its knees ?

If you know the answer, I'd love you to come discuss it with me on [my account](https://mastodon.social/web/accounts/15957)

### Another small thing: 

[Muto Shack](https://functional.cafe/@MutoShack) wrote an [introduction to Guile Scheme](https://muto.ca/planting-flowers-in-guile.html).

Ususally the scheme documentation is unusual, weird. I mean it's scientifical, theoretical, somewhat formal.

Not so Muto's one. It's somewhat naive in regard to the theoretical depths scheme delves into. 

But it has its place in the world. It's pragmatic, down to hearth, straight to the point. 

You will probably need to read and work some more after reading his introduction, if you want to do something real with Guile scheme. But if you never considered a lispy language in your life before, that's an excellent starting point

### a TO DO list for me and future directions for thsi blog too

I took [some notes](resources/da_fare.org) with org-mode. For now I will simply link it, because I have no org-mode parser for Guile scheme. I could borrow the markdown parser and convert it to deal with org-mode files (and that would be a further line to add to that file).

There are a few ideas that flashed through my mind in these days. 

Of course what's missing from there is the improvement to this blog. 

I'd like to 

  * give it a .onion address
  * publish it on ipfs too
  * give it some static pages, maybe about its tech setup, who I am, that sort of things
  * improvements to the layout (css ugh !)
  * some aid to navigation. A nav section ? A site map ? We'll see
  
Ok this was mostly a pointless (or "interlocutory") post. It's just that things keep crossing my mind and I needed to fixate them a bit.

That's all folks !!
