title: Guile and Free Software
date: 2018-04-04 10:00
tags: guile, autotools, free software, autoconf 
---

# Guile

Emacs is probably the most successful GNU project.

It' s difficult to list all the examples, as they are so many;  I' d just point to one: [org-mode](http://orgmode.org/ "link the org-mode home page")

org.mode is hugely successful, its mailing list is a maze, as far as I
understand some Google employees worked on it.

There are even mobile (native !) apps to edit org-mode files and
keeping them in sync with their counterparts on the pc, 
both for Android and for iOS.

The first person I met in Berlin, in one of those technophile
meetings, told me that org-mode was absolutely genial.

The idea behind Guile is to try to replicate that success.

Having a Scheme runtime, embedding general C based libs, 
so that they can be piloted from within a REPL.

And then unleash the community on it !

Now that you make me think about it, I remember I did something
similar with Clojure and Java libraries. 

With [Leiningen](https://leiningen.org/ "the Leiningen home page"), the clojure equivalent for Make, embedding a Java based library in a Clojure project is trivial.

Also, i did this in Clojurescript, embedding JavaScript libraries !!

[Here](http://catonano.altervista.org/ "link to a front end experiment I did some time ago with Hoplon")'s the result (push the datetime button on the bottom).

So the Guile idea on the paper it's a good idea ! To some extent it's battle tested !

Yet Guile was not as successful as Emacs. To say at least.

Why ?

Here, I'm gonna offer my take on the why, at least partially.

# Autoconf

Let's say that you want to wrap
[libpq](https://www.postgresql.org/docs/10/static/libpq.html "the
libpq page in the postgresql manual"), 
the C library to access Postgresql databases, with Guile.

Accessing relational databases from Basic was the bread and butter of
proprietary software and at today accessing Postgresql DBs from Guile
is wobbly and spotty at best. What a shame !!

So, in your GNU project skeleton, you'll need to fill in your `configure.ac` file.

Because Guile is a so good citizen of the GNU world, it provides some
macros for testing if Guile is available, where it is, and if some 
scheme module are available.

Such macros are duly documented in the manual, so that's covered.

But in order to access libpq you'd need a macro provided by libpq itself.

No such macro is provided.

Investigating Postgresql I was made aware that Postgresql provides an executable that can be called and it will return the path to the libs folder

So you should write a macro yourself that wraps such executable.

There's a PHP extension doing such a thing. I found it scouring on
line.

So I should have borrowed from that.

But I could barely understand it. What if I wanted to change a tiny bit ?

Have you ever seen the source of an Autoconf macro ?

You need to know:

  * the m4 macro language
  * bash scripting language (with its 2>&1 absurd things)
  * regular expressions, with their even more absurd things (like \\||!!"^§#çç!!)

You wanna offer the possibility to pilot C libs from a common high
level language ?

Fine, but you first have to learn all the ugly obsolete languages from
m4 up !! You, GNU non-believer !!

So now you're in a trouble sea. You have to shave a huge yak that
rolled itself in manure.

Wow that escalated fast !

## I am a noob but even experienced programmers loath Autoconf macros

Even my federation hero, [Christopher Lemmer Webber](https://dustycloud.org/) [wrote to me](https://mastodon.social/web/statuses/99796014455360735) on
Mastodon that he is overwhelmed by the Autotools

And he is a way more proficient programmer than I am.

I'll be as conceitful to believe that this is one of the reasons of
Guile not astounding result.

There are probably others that I'll try to address in further posts.

But you don't need this in order to play with Emacs lisp

And you surely don't need this in order to play with Java and
Clojure.

The friction is waaaay lower, there !!

And we still haven't tried to write a single line of code !!

Beware: code will be more difficult than the couple Clojure/Java because Java and Clojure are both garbage collected languages, the
semantic friction is lower, there.

In order to pilot C libs from Scheme, you need to worry about
bytevectors to contain pointers to your stuff and types (a pointer to
what ?) and then dereferencing and all that.

Those things won't hit you in the Clojure/Java broth. 

Well Guile/C has the marvelous [Bytestructures](https://github.com/TaylanUB/scheme-bytestructures "the github Page for Bytestructures") library that significantly eases the friction between Scheme and C, but as I said, that's material for another post.

So coming back to Automake: we aren't at writing code yet and we already feel like idiots ! 

Well I do.

## Skribilo

Another package I'd like to work on is [Skribilo](http://www.nongnu.org/skribilo/) and I have Autoconf related problems there too.

Skribilo is an ingenious publishing system made with Guile.

Currently it depends on guile 2.0 and guile-reader 2.0

You can see that its authors stopped working on it a few years ago.

I understand that it can publish the mathematical typesetting that you typically deal with in LaTex.

How does it publish it to the web ? Does it use mathml ?

In the past I published an [article](https://dl.dropboxusercontent.com/spa/rxkeykzi8c4mpm7/notes/public/index.html) using [MathJax](https://www.mathjax.org/) and it works well.

But it moves the processing to the client and this assumes some powerful clients !

I'd like to have a server side processing phase to publish my math type setting to the web.

I'd also like to explore the possibility to process the texinfo format maybe adding the possibility to publish math typesetting to texinfo too. In Guile, of course.

I read that some projects were moving to a python based documentation system because the texinfo machinery lacks this functionality.

So the step 0 would be to move the current Skribilo to depend on a current Guile (2.2.3 at the time of writing this post) and a current guile-reader.

The current configure.ac file sets up the dependencies and it lays out
a recursive processing compilation. It descends in subfolders and...

Some time ago this layout was deprecated and now you declare source
files explicitly in the root level of the project.

I should update that. But not knowing what you're doing is not exactly
rewarding and I'm somewhat procrastinating on that.

## G-golf
One more. 

I have an issue with the Autoconf macros regarding [G-golf](https://www.gnu.org/software/g-golf/ "G-golf home page") too.

G-golf is an exploratory attempt to wrap the Gnome desktop environment in Guile scheme.

It's not actively worked on admittedly but it´s a good step 0 nonetheless.

I managed to build G-golf with Guix by manually editing a source file in order to point to some Gnome related libraries in the Guix store.

I was suggested to do so by Ludo, on the Guix help mailing list

This solution, as I'll say more extensively later on, makes it work but in a sub optimal way. If I could set up the Autoconf macros in a way to make it work on Guix AND on other systems too it would be better

It's only a pity that apps for one of the most prominent Free Software desktops can't be made in the GNU extension language !

## Let's draw a hard conclusion

Autoconf is actively hampering my initiatives regarding Free Software projects. And I suspect I'm not the only one being hampered.

I'd love to try to provide integrations between Guile and other projects. 

And I'm not being held back by conceptual difficulties. But rather by technical debt.

Am I being presumptuous ? 

Maybe so. I take the responsibility.

So either I'm presumptuous or this is technical debt.

Let's call things with their names.

# A way out

In Guix package recipes, you can substitute some strings in source
files with some others.

In many packages there are, for example, hardwired paths
to resources.

In those cases, the Guix recipes substitute the hardwired paths in
the source files before compilation.

Well actually there are wrappers around binaries, and other gimmicks
too, the thing can become complicated quite fast there too.

But for my libpq it won't be.

In Guix package recipes, the dependencies of your package are
explicit.

You can easily indicate the correct path to a lib provided by
Postgresql.

The Guix recipe can rely on Autotools infrastructure, if it's there

But if it's not, they can do some things on their own. 

In Scheme.

You can process strings, parse, marshall, whatever you need.

Here's an example, from the efl package from the Enlightenment desktop
environment

```scheme
(add-after 'unpack 'hardcode-libcurl-location
           (lambda* (#:key inputs #:allow-other-keys)
		    (let* ((curl (assoc-ref inputs "curl"))
			   (lib  (string-append curl "/lib/")))
		      (substitute* "src/lib/ecore_con/ecore_con_url_curl.c"
				   (("libcurl.so.?" libcurl) ; libcurl.so.[45]
				    (string-append lib libcurl)))
		      #t)))

```


Of course a package like that won' t be available on other distros. Not
automatically at least

Other distros will have to solve the problem with their own
tools. That's a strain on packagers, for sure.

But hey I can't solve all alone, can I ?

It' s free software after all. If you want and can, you can write some
Autoconf macros yourself, I' ll gladly accept them.

Otherwise, install Guix in your distro. It's a supported option.

Well, you'll have to set your SELinux to permissive mode, as the Guix
SELinux policy is not ready yet, but that will be solved with time
