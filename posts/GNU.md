title: The GNU community
date: 2018-06-06 08:00
tags: GNU, Guile, Guix, Free Software, community 
---

# The GNU project
## Guix
[Guix](https://www.gnu.org/software/guix/) is the only GNU project whose I've met some people of.

I went to the FOSDEM in Brussel in 2017 and there was a room dedicated to Guix.

Even some guixers from the US were present. [Christopher Webber](https://dustycloud.org/), [Leo Famulari](https://www.leofamulari.com/) and some others.

As for people not from the US, I like to mention [Ludovic Courtes (aka Ludo)](https://en.wikisource.org/wiki/Author:Ludovic_Court%C3%A8s), [Ricardo Wurmus (aka Rekado)](https://elephly.net/), [Christopher Baines](https://www.cbaines.net/), [Manolis Ragkousis](http://www.manolisragkousis.com/).

We went to dinner and we talked a lot. I really whish we could do this more often.

A few months later, I went to a GNU Hacker Meeting in Germany. It was meant to be a GNU event but the large majority of attendants were from Guix.

Apart the ones from the US, mostly the same people attended the GNU HM.

The Guix community is probably the sanest community I have ever experienced.

Not only inside the GNU world, but in the FLOSS universe.

In general, the Guix community is the best one I have ever experienced.

In fact, Guix is the only FLOSS project that I have ever been able to contribute to.

Ok, [my patches](https://git.savannah.gnu.org/cgit/guix.git/log/?qt=author&q=catonano) were basic, I'd say elementary. But still they were accepted.

That's not a case. I was able to contribute because Guix was welcoming and empowering.

### Guix welcoming attitude

I need to mention a single episode that stuck with me.

In 2017 we had an accident on the guix dev mailing list.

Someone was hostile to a non gender binary member of the community.

The mantainers, Ludo and Rekado, were adamant in enforcing the code of conduct.

They were ready to manage the issue and they showed the right level of awareness.

That was a trial by fire, to me. 

I honesty expected them to excuse the offensive person, because they were a prolific contributor.

I was wrong. The words used were clear.

That offensive person has stopped contributing since then.

They have also been caught bad mouthing the guix community on the GNU irc channel

Another prolific hacker stopped contributing because they felt that the Guix community was "radical", along te lines of these deranged white priviledged people who feel "oppressed" by the enforcing of just rules.

I saw they are contributing to other GNU projects, now.

Like it sometimes happens with childs abusing clerics, if you are hostile to people in a GNU group, you can always move to another one.

That said, the Guix mantainers did the right thing, regardless of the cost for the community.

I' not able to contribute to Guix at the same technical level of these abusive people who went away, and Ludo and Rekado perfectly know this.

And yet, they made an investment in correctness.

Wow !!

In these days I observe the Guix mantainers correcting people in the IRC channel when they write "dudes" or "guys" and they correct them suggesting "folks" or "people" instead.

Every single time.

This is something that cannot be stressed too much. This is important !

I want to take this break to thank the Guix people for this. This is probably more important than the technology per se.

### some more reasons in favour of Guix

Apart from a few single episodes, another important thing is that in the Guix channels (the mailing lists and the IRC channel) you will never feel like an idiot.

You can ask whatever question will pass through your mind.

I have asked a lot of questions, sometimes silly ones.

I have interacted a lot.

I never ever felt unwelcomed.

ALL and every question will be answered kindly.

Sometimes it does take a while, but you will NEVER be administered the silence medicine on the Guix channels.

I never was.

And this has been going on for a couple of years now.

I can only be thankful for that.

All this might sound cheesy, but I am aware that it requires effort by the mantainers and I think it deserves to be acknowledged.

It doesn't happen everywhere.

## Now for the bad part

I'sorry to state that the same things cannot be said of the Guile channels.

You WILL be administered the medicine of silence on the guile channels and you will be reprimanded and you will be made feel like an idiot.

I was.

I was made feel like an idiot (last time very recently) and I was reprimanded.

I think I was reprimanded unfairly. 

2 times.

I kept a boomark to the log of the second time, I didn't keep a bookmark of the log of the first time.

But it was 2 times.

### some background
Before going deeper in my accidents on the Guile channels, I'd like to lay out some background.

A few days ago apolemic erupted on line about an infortunate joke in a GNU manual and the RMS's position about it.

In the midst of the discussion, some people made the more general argument that the GNU group as a whole is a toxic community.

See [this](https://twitter.com/davexunit/status/994221608335224833 "a tweet by David Tompson") tweet.

I was upset back then, but now I understand. I'm afraid I agree. The GNU group is toxic.

I also want to point my readers to some resources regarding FLOSS projects communities that I think are relevant to my discussion.

#### the first talk
The first one is [The Dark Side Of Free Software Communities](https://media.libreplanet.org/u/libreplanet/m/the-dark-side-of-free-software-communities/ "The Dark Side Of Free Software Communities, the talk")

This is an enlightening talk. I didn't have the pleasure to know the author personally, but I think they made a stellar performance with this talk.

They illustrate how some toxic characters hurt people in the community, by gatekeeping, telling them they're wrong, being dismissive.

In this talk you'll hear sentences like: "And people walk away, because we hurt them" or "admit we have been abusive" or "admit that our software sometimes sucks"

I would love to embed the video in this very page for you to watch it without leaving, but I don't know how, because I'm using a Free Software Guile based blog editor and sometimes Free Software sucks ;-)

But please, please, go to watch it and then, if you want, come back and finish reading this :-)

#### the second talk

The second one is again from the Libreplanet 2018.

It's [Sharing strategies for welcoming newcomers into FLOSS projects: First-timers-only, list moderation, and more ](https://media.libreplanet.org/u/libreplanet/m/sharing-strategies-for-welcoming-newcomers-into-floss-projects-first-timers-only-list-moderation-and-more/ "Sharing strategies for welcoming newcomers into FLOSS projects: First-timers-only, list moderation, and more ")

In this talk you'll hear sentences like: "if there are places where people come to your community or into your project and they experience unfriendliness, or the lack of friendliness, I think it's worth that you take that really seriously" (17:40)

Or like "[the use of emojis] is not like icing on a cake, but there's something fundamental about using lots of emojis".

The same applies here. Go watch it, then come back :-)

#### now the Stackoverflow related references

There are some explicative references about bad attitudes associated with [Stackoverflow](https://stackoverflow.com/ "Stackoverflow")

The first one is [this (sorry, it's on Medium)](https://medium.com/@Aprilw/suffering-on-stack-overflow-c46414a34a52)

This is enlightening about how much hardship people have to go trhough in tech communities and how much emotional labour it takes to tackle the issue.

Look at how people tweeted in reply to an argument by the author of this piece.

Somehow it seems that you need to be compassionate towards your bullies sort of like what Jesus taught (I'm an atheist, the citation is entirely historical).

This is something that I see again and again in black people campaigning. 

You denounce an awful thruth but you'll be called a monster because your ways were not entirely empathetic.

Sounds absurd but that's exactly what goes on usually.

Now, at least the Stackoverflow top brass [conceded](https://stackoverflow.blog/2018/04/26/stack-overflow-isnt-very-welcoming-its-time-for-that-to-change/?cb=1) that there´s room for improvement and the improvement needed is not a technological one, but rather about behavior of people on the site.

#### the last little push

Please, bear with me. A little last effort and we're done with the background !!

This talk is from FOSDEM 2017, it has no video available but the synopsys is clear enough. An excerpt: 

"Documentation, and technical support in general, sets the tone for your community, in that it determines who sticks around. If you're a jerk, the next generation of your community will be composed of jerks."

[Take a look](https://archive.fosdem.org/2017/schedule/event/better_manual/) yourself

Again from FOSDEM 2017, [this](https://archive.fosdem.org/2017/schedule/event/legacy_docs/ "a talk") talk about documentation.

The key point of the talk is that the documentation usually illustrates the technical features of a project.

And the process of envisioning solutions for use cases is left to the reader.

Instead, the documentation should start from specific use cases. It should be an anthology of use cases.

This picture can be used as a starting point.
![One of the slides of the talk](resources/images/cazz.jpg) 

For example I hadn't realzed that I could use the feature to evaluate an Emacs buffer with Guile in order to do some exploratory development

The manual mentions that of course, but I couldn't connect the dots

David Thompson suggested me this on Mastodon.

And this is a small example.

Another one could be that I' not sure about reading and writing files in Guile, maybe files that are larger than the memory the computer has so they should be read/written lazily.

I [made](https://github.com/humanitiesNerd/openArpa "my Clojure based project") such a thing in Cloure and I'd love to replicate it in Guile scheme.

The manual mentions several features related to that all around but I have a hard time in extracting a solution for this use case.

Right now I'm having a more substantial issue with Guile, more on that in a few lines.

Finishing off this humongous background laying out, I think [It’s Time to Retire “RTFM” (this one's on Medium too, sorry)](https://medium.com/compassionate-coding/its-time-to-retire-rtfm-31acdfef654f) and try some more compassionate alternatives instead.

### Ok the laying out of the background is over

I'll start from the most recent incident.

In Guix, among other very interesting things, there is a layer of software that abstracts the access to a daemon that, in its own turn, accesses the file system and a relational database.

The daemon mediates the processing of software packages.

In Guix you don't access the file system yourself, you go thrrough the mediation of such daemon.

Roughly like in memory managed languages you don't access the memory yourself, you go through the mediation of the runtime that allocates and frees memory for you.

The layer in Guix is made with a so called monad.

That means that you enter in a state (at the REPL or programmatically) such that you call procedures that manipulate the packages store (calling the daemon) and the store is _implicit_

That is, you don't pass the store as an argument to the _monadic_ procedures that process it. The store is _implicit_

Pay attention ! The writings operations in the store could depend on the _order_ that you follow in doing them.

And I still don't understand how the Guix layer deals with that

Ok, I never considered learning Haskell. I would probably already know how this monad works, if I had.

But I don't.

This monad is implemented as a macro (or a set of macros)

So, I thought, I will use the macroexpand-1 facility, in order to dissect this thing and try to understand how it's done and what it does.

The macroexpand-1 procedure (or macro, I don't know) expands a macro but not completely. It only performs a single expansion step.

So you can see the several steps. Pretty much like in a debugger.

That may help you in understanding.

In the NEWS file in the Guile root there's a note that says that macroexpand-1 has been eliminated because it's not clear how to make it workk with the Guile macro facilities.

Macro expanding alone is not enough because an expanded macro is often very far from the not expanded form, you need to see the steps, in order to understand.

I posted a question on Stackoverflow and almost instantly a kind soul posted a solution that works in Racket

This one

``` scheme
#lang racket

(define-syntax (expand1 stx)
  (syntax-case stx ()
    [(_expand1 form)
     (syntax-case #'form ()
       [(id . more)
        (identifier? #'id)
        (let ([transformer (syntax-local-value #'id)])          
          (with-syntax ([expansion (transformer #'form)])
            #''expansion))]
       [_
        #''form])]))
```


If you test this scrap of code on 

``` scheme
(expand1 (or 1 2 3))
```

You get

```scheme

'(let ((or-part 1)) (if or-part or-part (or 2 3)))

```

It seems what I needed

If you take a few seconds to read this code, you'll see that the most important bit is this ``` syntax-local-value``` procedure (or macro) that extracts the _transformer_ of a macro

You don't know what a transformer is ? 

Then you should probably read [this](https://www.cs.indiana.edu/~dyb/pubs/bc-syntax-case.pdf "the academic article about hygienic macro expanding in scheme") article about scheme hygienic macro expanding

I read it until I found that the macro expanding system is imlemented as a set of macros. 

It assumes a previously developed syntax macro system.

It's an implementation detail.

Geez.

Anyway, is ```syntax-local-value``` available in Guile ?

The manual doesn't even mention macro stepping. It mentions macro expanding only.

Isn't this a implementation specific facility ? 

The answer is, yes it is.

In fact, if you look for it in the manual, you won't find it

In the manual there's a ```syntax-local-binding``` that does basically the same thing.

It's in a paragraph titled _Syntax transformer helpers_, in the chapter about macros.

But those, as explained in the article, are meant to provide meaningful error messages to the users of your macros. The helpers are meant to be a part of the macro expansion system, not an internal implementation facility.

Yet, the discussion of the internals are in that paragraph.

And admittedly, that paragraph explains the choices of how to implement macroexpansion in Guile and why.

This is the relevant bit, before moving to describe ```syntax-local-binding```


_Guile also offers some more experimental interfaces in a separate
module.  As was the case with the Large Hadron Collider, it is unclear
to our senior macrologists whether adding these interfaces will result
in awesomeness or in the destruction of Guile via the creation of a
singularity.  We will preserve their functionality through the 2.0
series, but we reserve the right to modify them in a future stable
series, to a more than usual degree._


This is pretty much what Mark H Weaver wrote to me on the guile user mailing list replying to my question about what the problem was with macroexpand-1.


Before reporting about some really ugly shit, I want you to notice that Racket [has](https://docs.racket-lang.org/macro-debugger/index.html "Racket macro stepping and inspecting GUI tools") a facility for macro stepping, macro inspecting (whatever that means) and that such facilities have a wonderful GUI, you don't need to fumble with Emacs

### now for some really ugly shit

So, I posted a question on the Guile user mailing list about what the problem was with macroexpand-1

I assumed that the plain vanilla basic step for having a macro stepping facility, was macroexpand-1

I waited a few days. I wrote to Andy Wingo a private message on Twitter and got no answer.

I waited a few more days then Mark H Weaver interacted. 

In a first phase, he was kind and effective.

Actually he explained to me how a macro stepper is meant to be implemented and what the problem is with macroexpand-1

He also mildly annoyedly explained to me that my assumption that macroexpand-1 is the basic step in building a macro stepper was wrong.

I apologized for _my_ mistake assuming good faith on his side.

He was the only one providing any help so I was interested in keeping the relationship.

This is the first signal of toxic interaction. Gatekeeping and slight shaming from a position of power.

Anyway he provided useful information and I even praised him publicly, as you can see [here](https://mastodon.social/@catonano/100124409131988979 "link to the thread on Mastodon")

I replied to him as follows


_This is surprising to me. When I saw that example made in Racket for the first time I instantly identified "syntax-local-value" as problematic. ´Will Guile have anything equivalent ?` I asked myself. Now you show me the "(system syntax)" namespace (or module) I didn't  suspect it existed. Does the manual mention it anywhere ? I didn' t see it. Or maybe does it belong to any scheme standard ? Do any more (system ....) namespaces exist ? How would I know ?_

I repeat the question: does the manual mention the (system syntax) module anywhere ? 

The answer is yes. It mentions it in the paragraph that I indicated earlier.

How would I have known about it ?

After all it's in a paragraph about macro helpers not being an helper, not in the sense of the article, and the manual doesn't mention macro stepping.

But at this point Mark's patience was exhausted and he stopped replying to me.

He moved on replying to other threads in the mailing list.

This reminds me of a Walt Disney movie in which a group of sharrs participate in a self awareness group, they sit in a circle and repeat a mantra to themselves: 

> "you don't eat other fishes
> 
> you don't eat other fishes
> 
> you don´t eat other fishes"

So it goes on GNU groups

They try to be human

probably they repeat to themselves

> don't be a smug jerk to people.
> 
> don't be a smug jerk to people.
> 
> don't be a smug jerk to people.

but then they fall. Because it's their nature.

Even when they reply to your questions, they manage to make you feel like an idiot.

They really have a knack for this, they are very effective !

Mark was probably tempted to say Read The Fine Manual, but he refrained, because they have learned at least that much in so many years.

Do you see the difference between the Racket experience and the Guile experience ?

I read an academic article, I waited for days, I had to accept not being answered to, I had to apologize for not knowing the difference between macro stepping and macroexpand-1

Days of work and swallowing jerky behaviour for solving a thing that in Racket is a no brainer.

So, let me wrap up a bit:

  * The docs is focused on the tech features, assuming that the reader knows how they relate to use cases.

  * The people barely manages to not "RTFM" you.

  * Having annoyedly corrected you about formal mistakes you made in good faith

  * not having the minimum consideration for your effort and the time you devolve in learning their lousy projects

ALL THESE THINGS were clearly discussed in the talks I indicated, so these are no news at all

Please take a minute to let this sink in you.

### one more toxic episode

When I wrote [the previous post](http://catonano.v22018025836661967.nicesrv.de/guile-and-free-software.html) in this blog, I was assaulted on the Guile IRC channel because, apparently, my post was not well received by one of the Guile community member.

I was told I was hoplessly wrong, unfair, unwilling to learn.

I was made aware that the assailant is known in the community as a jerk and that he even annoys the mantainers sometimes.

But in that discssion also some other bystanders tried to convince me, with gentler terms, that the assailant was right, after all.

So, as the FOSDEM 2017 talk correctly argued, it rarely a single jerk. It's more probably a group of jerks.

You can read [the discussion](https://gnunet.org/bot/log/guile/2018-04-12#T1676065) in all its glory.

### the last toxic episode

I have no boomark of this one

But I was reprimanded by a "prolific" contributor because I had made some formal mistakes.

I don't remember his name but it doesn't matter.

I was just trying to elicit a bit more interaction, given how the Guile groups are depressive.

## consequences of the Guile community toxicity 

Guile is a failure.

The main idea was that Guile should have been to C based software what Elisp is to the Emacs kernel.

Many projects would have offered the possibility to script a kernel a la Emacs.

This project failed miserably.

The collection of software made with Guile is over meager. It's ridiculous.

The Guile community is asphyctic. The mailing list is full of unanswered questions.

Maybe, as it happened to me, the authors of those questions were not smart enugh to be considered. Maybe not. I don't know.

Nothing meaningful is made with Guile except for Guix.

The main strenght point of Guix in regard to Guile is not the technology. It's the humanity.

And anyway, the founder of Guix, Ludo, is a former Guile mantainer.

Is there any successful project made with Guile by any outsider ? I'm not aware of any.

Guile is not for everybody.

Guile is the realm of smugness and elitarism.

The history of Guile is quite long, we're talking about decades.

In this time Python, Ruby, Javascript, R came along. In many of them, you can extend C based software.

The projects made with those languages often have thousands of contributors and millions of users.

And they are all Turing equivalent, together with Guile.

What made the difference is not the technology.

## where does this leave Guix ?

Guix here is the white fly. It's the exception.

Ludo obviously understands the issue and the tone in Guix is deeply different.

Maybe he wants to school other GNU projects: "see ? This is how it's done"

Maybe, even being the authoritative person that he is in that community, he still can't afford to plainly tell his mates that they are a bunch of jerks.

His attempt is very generous and also quite successful.

In fact, not only it's the only project I ever contributed to

But the most lovely objections to my grudges on Mastodon came by Christopher Baines and Christopher Webber. 

2 Guix people.

An invitation to not be discouraged and keep blogging about Guix and Guie came from Arun Isaac, another Guix person.

An inquiry about the details of one of my incidents came from David Thompson, another Guix person.

The Guix community is something else

Because Ludo and Rekado built it this way.

Yet, right now, I wouldn't feel comfortable in suggesting Guix to anyone. Because you can't be involved in Guix without being exposed to the Guile toxicity.

Not everyone has some large shoulders like Ludo and Rekado and I don´t feel like suggesting people to go in a place where they coud be hurt.

I'd rather suggest Racket, [Pharo](https://pharo.org/ "Pharo"), and a traditional Linux distribution. 

It's a pity and it's sad, you might object

And you might be right
