(use-modules (haunt asset)
             (haunt site)
             ;(haunt builder blog)
             (scheme builder blog) ;;<-- the builder should come from my module !
	     (scheme reader texinfo)
             ;(haunt reader texinfo)
             (haunt builder atom)
	     (haunt builder assets)
	     (haunt reader)
             (scheme reader skribe)
	     (haunt reader commonmark)
             (haunt asset))


;(define (add-to-load-path* directory)
;  (unless (member directory %load-path)
;    (add-to-load-path directory)))

;(add-to-load-path* "/home/catonano/haunt-blog/scheme/")

(site #:title "Quisquiliae"
      #:domain "https://humanitiesnerd.gitlab.io"
      
      #:default-metadata
      '((author . "Catonano")
        (email  . "catonano@gmail.com"))
      #:readers (list skribe-reader texinfo-reader commonmark-reader)
      #:builders (list
		  (static-directory 
				    "resources"
				    "resources"
				    )
		  (my-blog ) ;; <-- the builder should be my-blog
		  (atom-feed)
		  (atom-feeds-by-tag)
		  ))

;;(make-asset "quisquilie/styles/quisqiliae.css" "quisquilie.css")
